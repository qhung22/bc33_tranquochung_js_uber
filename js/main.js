const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhGiatienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
}

function tinhGiaTienKM1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}

function tinhGiaTienKM19trodi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
    default:
      return 0;
  }
}

function tinhTientimeCho(car) {
  switch (car) {
    case UBER_CAR: {
      return 2000;
    }
    case UBER_SUV: {
      return 3000;
    }
    case UBER_BLACK: {
      return 3500;
    }
    default:
      return 0;
  }
}

//main function
function tinhTienUber() {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  console.log("carOption: ", carOption);

  var giaTienKMDauTien = tinhGiatienKmDauTien(carOption);
  console.log("giaTienKMDauTien:", giaTienKMDauTien);

  var giaTienKM1_19 = tinhGiaTienKM1_19(carOption);
  console.log("giaTienKM1_19:", giaTienKM1_19);

  var giaTienKM19trodi = tinhGiaTienKM19trodi(carOption);
  console.log("giaTienKM19trodi: ", giaTienKM19trodi);

  var giaTientimecho = tinhTientimeCho(carOption);
  console.log("giaTientimecho:", giaTientimecho);
  var soKM = document.getElementById("txt-km").value * 1;
  if (soKM <= 1) {
    tienTra = giaTienKMDauTien * soKM;
  } else if (soKM <= 19) {
    tienTra = giaTienKMDauTien + (soKM - 1) * giaTienKM1_19;
  } else if (soKM > 19) {
    tienTra =
      giaTienKMDauTien + 18 * giaTienKM1_19 + (soKM - 19) * giaTienKM19trodi;
  }
  document.getElementById("xuatTien").innerHTML = `${tienTra}`;
}
//
